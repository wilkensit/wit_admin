<?php

/**
 * Implements hook_preprocess_html
 *
 * Adds Font Awesome
 */
function wit_admin_preprocess_html(&$variables) {
  drupal_add_css(
    '//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
    array('type' => 'external')
  );
}

/**
 * Implements hook_form_node_form_alter.
 *
 * Changes the vertcal tab order on node add/edit forms
 */
function wit_admin_form_node_form_alter(&$form, &$form_state, $form_id) {
  $form['revision_information']['#weight'] = -10;
  $form['author']['#weight'] = -9;
  $form['options']['#weight'] = -8;
}

/**
 * Implements hook_date_nav_title
 *
 * Override the calendar title.
 */
function wit_admin_date_nav_title($params) {
  $view = $params['view'];
  $date_info = $view->date_info;
  $format = !empty($params['format']) ? $params['format'] : NULL;
  $granularity = $params['granularity'];
  switch ($granularity) {
    case 'year':
      $title = $date_info->year;
      break;
    case 'month':
      $format = !empty($format) ? $format : 'F Y';
      $title = date_format_date($date_info->min_date, 'custom', $format);
      break;
    case 'day':
      $format = !empty($format) ? $format :
        (empty($date_info->mini) ? 'l, F j, Y' : 'l, F j');
      $title = date_format_date($date_info->min_date, 'custom', $format);
      break;
    case 'week':
      $format = !empty($format) ? $format :
        (empty($date_info->mini) ? 'F j, Y' : 'F j');
      $title = t('Week of @date', array(
        '@date' => date_format_date($date_info->min_date, 'custom', $format)
      ));
      break;
  }
  return $title;
}
